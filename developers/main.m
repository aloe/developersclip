//
//  main.m
//  developers
//
//  Created by kawase yu on 2014/09/01.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
