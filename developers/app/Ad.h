//
//  Ad.h
//  smart2ch
//
//  Created by kawase yu on 2014/08/11.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AfiArticleList.h"
#import "Site.h"

@interface Ad : NSObject

+(Ad*)getInstance;
-(void) setup:(UIViewController*)rootViewController_;
-(void) reload;
-(AfiArticle*)afiArticle;
-(AfiArticleList*) afiArticleList;
-(Site*)adSite;

@end
