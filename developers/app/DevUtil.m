//
//  DevUtil.m
//  developers
//
//  Created by kawase yu on 2014/09/01.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "DevUtil.h"
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import <CoreTelephony/CTCarrier.h>
#import "GAI.h"
#import "GAITracker.h"
#import "GAITrackedViewController.h"
#import "GAIDictionaryBuilder.h"
#import "GAIFields.h"
#import "GAILogger.h"

@interface DevUtil ()

+(void) noReview;

@end
@interface AlertDelegate : NSObject<UIAlertViewDelegate>

@end

@implementation AlertDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(buttonIndex == 0){
        // レビューする
        [VknUtils openBrowser:@"https://itunes.apple.com/jp/app/id916134898?l=ja&ls=1&mt=8"];
        [DevUtil noReview];
    }else if(buttonIndex == 1){
        // あとで
        return;
    }else if(buttonIndex == 2){
        // レビューしない
        [DevUtil noReview];
    }
}

@end


@implementation DevUtil

static UIScrollView* mainSv;
+(void) setup:(UIScrollView*)mainSv_{
    mainSv = mainSv_;
    // トラッキング設定
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    [GAI sharedInstance].dispatchInterval = 20;
    //    [[[GAI sharedInstance] logger] setLogLevel:kGAILogLevelVerbose];
    [[[GAI sharedInstance] logger] setLogLevel:kGAILogLevelNone];
    [[GAI sharedInstance] trackerWithTrackingId:@"UA-54510091-1"];
}

+(UIScrollView*)mainSv{
    return mainSv;
}

+(float) articleHeight{
    return ([VknDeviceUtil windowHeight] - 60 ) / 3.0f;
}

+(void) addGlobalEventlistener:(id)target selector:(SEL)selector name:(NSString*)name{
    NSNotificationCenter* c = [NSNotificationCenter defaultCenter];
    [c addObserver:target selector:selector name:name object:nil];
}

+(void) dispatchGlobalEvent:(NSString*)name{
    NSNotification *n = [NSNotification notificationWithName:name object:nil];
    [[NSNotificationCenter defaultCenter] postNotification:n];
}

+(BOOL) isJaDevice{
    CTTelephonyNetworkInfo *netinfo = [[CTTelephonyNetworkInfo alloc] init];
    CTCarrier *carrier = [netinfo subscriberCellularProvider];
    NSString* countryCode = carrier.mobileCountryCode;
    return ([countryCode isEqualToString:@"440"] || [countryCode isEqualToString:@"441"]);
}

static NSString* showedCountKey = @"showedCountKey";
static AlertDelegate* alertDelegate ;
+(void) tryShowRecommend{
    NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
    int current = [ud integerForKey:showedCountKey];
    current++;
    [ud setInteger:current forKey:showedCountKey];
    [ud synchronize];
    
    if([self isNoReview]) return;
    if(current != 5 && current != 10 && current != 30) return;
    
    UIAlertView* alertView = [[UIAlertView alloc] init];
    alertView.title = @"ご利用ありがとうございます";
    alertView.message = @"ご意見ご感想ありましたらぜひお気軽にレビューより投稿くださいm(^_^)m。";
    [alertView addButtonWithTitle:@"レビューする"];
    [alertView addButtonWithTitle:@"あとで"];
    [alertView addButtonWithTitle:@"レビューしない"];
    
    alertDelegate = [[AlertDelegate alloc] init];
    alertView.delegate = alertDelegate;
    
    [alertView show];
}

static NSString* noReviewKey = @"noReviewKey";
+(BOOL) isNoReview{
    return [[NSUserDefaults standardUserDefaults] boolForKey:noReviewKey];
}

+(void) noReview{
    NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
    [ud setBool:YES forKey:noReviewKey];
    [ud synchronize];
}

+(void) trackScreen:(NSString *)screenName{
    id tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName
           value:screenName];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
}

+(void) trackEvent:(NSString *)category action:(NSString *)action label:(NSString *)label value:(NSNumber *)value{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:category     // Event category (required)
                                                          action:action  // Event action (required)
                                                           label:label          // Event label
                                                           value:value] build]];    // Event value
}


@end
