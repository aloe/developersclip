//
//  Mediator.m
//  developers
//
//  Created by kawase yu on 2014/09/01.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "Mediator.h"
#import "Model.h"
#import "VknPostCommand.h"
#import "Api.h"
#import "RootViewController.h"
#import "LoadingViewController.h"
#import "Pager.h"
#import "Page.h"
#import "Ad.h"

@interface Mediator ()<PageDelegate, UIScrollViewDelegate>{
    UIWindow* window;
    RootViewController* rootViewController;
    LoadingViewController* loadingViewController;
    UIScrollView* sv;
    NSArray* pageList;
    UIView* hideView;
    Pager* pager;
    int loaded;
    int currentPage;
}

@end

@implementation Mediator

-(id) initWithWindow:(UIWindow*)window_{
    self = [super init];
    if(self){
        window = window_;
        [self initialize];
    }
    
    return self;
}

-(void) initialize{
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    rootViewController = [[RootViewController alloc] init];
    rootViewController.view.backgroundColor = [UIColor blackColor];
    window.rootViewController = rootViewController;
    [[Ad getInstance] setup:rootViewController];
    
    hideView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, [VknDeviceUtil windowHeight])];
    [rootViewController.view addSubview:hideView];
    
    sv = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 320, [VknDeviceUtil windowHeight])];
    sv.delegate = self;
    sv.pagingEnabled = YES;
    sv.showsVerticalScrollIndicator = NO;
    [rootViewController.view addSubview:sv];
    
    pager = [[Pager alloc] init];
    [rootViewController.view addSubview:[pager getView]];
    
    loadingViewController = [[LoadingViewController alloc] init];
    [rootViewController.view addSubview:loadingViewController.view];
    [loadingViewController startLoading];
    
    [DevUtil addGlobalEventlistener:self selector:@selector(onShowArticle) name:ON_SHOW_ARTICLE_EVENT];
    [DevUtil addGlobalEventlistener:self selector:@selector(onHideArticle) name:ON_HIDE_ARTICLE_EVENT];
    
    Model* model = [Model getInstance];
    if([model isDataInitialized]){
        NSLog(@"second ");
        [[Model getInstance] setup:^{
            if([DevUtil isJaDevice]){
                [[[Model getInstance] siteList] add:[[Ad getInstance] adSite]];
            }
            [self setViews];
            [self updateViews];
            [self updateModel];
            [loadingViewController endLoading];
        }];
        
        return;
    }
    
    [model updateModel:^{
        [model setup:^{
            if([DevUtil isJaDevice]){
                [[[Model getInstance] siteList] add:[[Ad getInstance] adSite]];
            }
            
            [self setViews];
            [self updateViews];
            [loadingViewController endLoading];
        }];
    }];
}

-(void) onShowArticle{
    sv.scrollEnabled = NO;
    [pager hide];
    
}

-(void) onHideArticle{
    sv.scrollEnabled = YES;
    [pager show];
}

-(void) setViews{
    NSLog(@"setViews");
    [[Ad getInstance] reload];
    
    SiteList* siteList = [[Model getInstance] siteList];
    
    [DevUtil setup:sv];
    
    NSMutableArray* tmp = [[NSMutableArray alloc] init];
    for(int i=0,max=[siteList count];i<max;i++){
        Page* page = [[Page alloc] initWithDelegate:self];
        UIView* pView = [page getView];
        pView.transform = CGAffineTransformMakeTranslation(0, i*[VknDeviceUtil windowHeight]);
        [hideView addSubview:pView];
        [sv addSubview:pView];
        [tmp addObject:page];
    }
    pageList = [NSArray arrayWithArray:tmp];
}

-(void) updateViews{
    SiteList* siteList = [[Model getInstance] siteList];
    sv.contentSize = CGSizeMake(320, [VknDeviceUtil windowHeight]*[siteList count]);
    
    int p = sv.contentOffset.y / sv.frame.size.height;
    [pager reload:siteList];
    [pager active:p];
    
    NSMutableArray* tmp = [pageList mutableCopy];
    for(int i=0,max=[siteList count];i<max;i++){
        Site* site = [siteList get:i];
        Page* page;
        if(i >= [pageList count]){
            page = [self createPage:i];
            [tmp addObject:page];
        }else{
            page = [pageList objectAtIndex:i];
        }
        
        [page registerModel:site];
        if(i < 2){
            [page reload];
        }
    }
    
    pageList = [NSArray arrayWithArray:tmp];
}

-(Page*)createPage:(int)index{
    Page* page = [[Page alloc] initWithDelegate:self];
    UIView* pView = [page getView];
    pView.transform = CGAffineTransformMakeTranslation(0, index*[VknDeviceUtil windowHeight]);
    [sv addSubview:pView];
    
    return page;
}

-(void) updateModel{
    [[Model getInstance] updateModel:^{
        NSLog(@"Updated");
        [[Model getInstance] setup:^{
            if([DevUtil isJaDevice]){
                [[[Model getInstance] siteList] add:[[Ad getInstance] adSite]];
            }
            [self updateViews];
        }];
    }];
}

-(void) updateModelActive{
    [[Model getInstance] updateModel:^{
        NSLog(@"Updated");
        [[Model getInstance] setup:^{
            if([DevUtil isJaDevice]){
                [[[Model getInstance] siteList] add:[[Ad getInstance] adSite]];
            }
            [self updateViews];
            int p = sv.contentOffset.y / sv.frame.size.height;
            int prev = p-1;
            int next = p+1;
            if(prev >= 0){
                [(Page*)[pageList objectAtIndex:prev] reload];
            }
            if(next < [[[Model getInstance] siteList] count]){
                [(Page*)[pageList objectAtIndex:next] reload];
            }
            [(Page*)[pageList objectAtIndex:p] reload];
            
        }];
    }];
}

-(CGRect)pageFrame{
    return CGRectMake(0, 0, 320, [VknDeviceUtil windowHeight]);
}


#pragma mark -------- UISCrollViewDelegate -----------

#pragma mark -------- UIScrollViewDelegate -----------

-(void) scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    scrollView.scrollEnabled = YES;
    
    float y = scrollView.contentOffset.y;
    int p = y / scrollView.frame.size.height;
    
    BOOL isPrev = (p < currentPage);
    BOOL isNext = (p > currentPage);
    currentPage = p;
    
    Page* page = [pageList objectAtIndex:p];
    UIView* pageView = [page getView];
    [page dark:0.0f];
    
    int next = p+1;
    int prev = p-1;
    if(isPrev && prev >= 0){
        Page* page = (Page*)[pageList objectAtIndex:prev];
        [page reload];
    }
    else if(isNext && next < [[[Model getInstance] siteList] count] /* && next > loaded*/){
        Page* page = (Page*)[pageList objectAtIndex:next];
        [page reload];
    }
    
    [pager active:p];
    
    if(pageView.superview == sv) return;
    
    CGRect pageFrame = [self pageFrame];
    pageFrame.origin.y  = pageFrame.size.height * p;
    pageView.frame = pageFrame;
    [sv addSubview:pageView];
}

-(void) scrollViewDidScroll:(UIScrollView *)scrollView{
    float y = scrollView.contentOffset.y;
    float svHeight = scrollView.frame.size.height;
    int p = y / svHeight;
    int amari = (int)y % (int)svHeight;
    
    float per = (float) amari / svHeight;
    float scale = 1.0f- (per / 6.0f);
    
    CGRect pageFrame = [self pageFrame];
    Page* page = [pageList objectAtIndex:p];
    UIView* pageView = [page getView];
    
    if(pageView.superview != hideView){
        [hideView addSubview:pageView];
        pageView.frame = pageFrame;
    }

    pageView.transform = CGAffineTransformMakeScale(scale, scale);
    [page dark:1.0f-scale];
    
    p = (y+svHeight/2.0f) / svHeight;
}

-(void) scrollViewWillBeginDecelerating:(UIScrollView *)scrollView{
    scrollView.scrollEnabled = NO;
}

#pragma mark -------- -----------

-(void) willEnterForeground{
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    [[Ad getInstance] reload];
    [self updateModelActive];
    
    [DevUtil tryShowRecommend];
}

-(void) didEnterBackground{
    
}

-(void) application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken{
    NSString* deviceTokenStr = [VknDataUtil deviceTokenToStr:deviceToken];
    NSLog(@"deviceTokenStr:%@", deviceTokenStr);
    
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    NSDictionary* param = @{
                            @"devicetoken": deviceTokenStr
                            , @"version": version
                            };
    
    NSString* registerUrl = [Api url:@"registerToken.php"];
    VknPostCommand* c = [[VknPostCommand alloc] initWithValues:param withUrl:registerUrl];
    c.onComplete = ^(NSData* data){
        NSLog(@"%@", [VknDataUtil dataToString:data]);
    };
    c.onFail = ^(NSError* error){
        NSLog(@"fail:%@", error.debugDescription);
    };
    [c execute:nil];
    
    NSLog(@"register:%@", [Api url:@"registerToken.php"]);
}


-(void) didReceiveMemoryWarning{
    [VknImageCache clear];
}

@end
