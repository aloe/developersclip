//
//  DevUtil.h
//  developers
//
//  Created by kawase yu on 2014/09/01.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import <Foundation/Foundation.h>

#define ON_SHOW_ARTICLE_EVENT @"ON_SHOW_ARTICLE_EVENT"
#define ON_HIDE_ARTICLE_EVENT @"ON_HIDE_ARTICLE_EVENT"

#define ADSTIR_MEDIA_ID @"MEDIA-ca1b98e0"
#define ADSTIR_SPOT_ID 2
#define ADSTIR_SPOT_ID_MINI 1

@interface DevUtil : NSObject

// global
+(void) setup:(UIScrollView*)mainSv_;
+(UIScrollView*)mainSv;

+(float) articleHeight;

+(void) addGlobalEventlistener:(id)target selector:(SEL)selector name:(NSString*)name;
+(void) dispatchGlobalEvent:(NSString*)name;

+(BOOL) isJaDevice;

+(void) tryShowRecommend;

+(void) trackScreen:(NSString *)screenName;
+(void) trackEvent:(NSString *)category action:(NSString *)action label:(NSString *)label value:(NSNumber *)value;

@end
