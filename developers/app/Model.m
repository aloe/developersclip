//
//  Model.m
//  developers
//
//  Created by kawase yu on 2014/09/01.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "Model.h"
#import "Db.h"
#import "VknUrlLoadCommand.h"
#import "Api.h"
#import "AfiArticleList.h"

@interface Model(){
    SiteList* siteList;
    ArticleList* articleList;
    AfiArticleList* afiArticleList;
}

@end

@implementation Model

static NSString* lastupdatedKey = @"lastupdatedKey";
static NSString* afiStringKey = @"afiStringKey";

+ (Model*)getInstance {
    static Model* sharedSingleton;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedSingleton = [[Model alloc]
                           initSharedInstance];
    });
    return sharedSingleton;
}

- (id)initSharedInstance {
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (id)init {
    [self doesNotRecognizeSelector:_cmd];
    return nil;
}

static NSString* isDataInitializedKey = @"isDataInitializedKey";

-(BOOL) isDataInitialized{
    return [[NSUserDefaults standardUserDefaults] boolForKey:isDataInitializedKey];
}

-(SiteList*)siteList{
    return siteList;
}

-(void) setup:(ModelCallback_t)callback{
    [VknThreadUtil threadBlock:^{
        [[Db getInstance] setup:^(FMResultSet *siteResult, FMResultSet* articleResult) {
            siteList = [[SiteList alloc] initWithResults:siteResult];
            articleList = [[ArticleList alloc] initWithResults:articleResult];
            [self merge];
            
            NSString* afiString = [[NSUserDefaults standardUserDefaults] stringForKey:afiStringKey];
            if(afiString != nil && [DevUtil isJaDevice]){
                NSDictionary* afiDic = [VknDataUtil strToJson:afiString];
                afiArticleList = [[AfiArticleList alloc] initWithAfiArray:[afiDic objectForKey:@"created"]];
                Site* afiSite = [Site createAfiSite];
                afiSite.articleList = afiArticleList;
                for(int i=0,max=[afiArticleList count];i<max;i++){
                    AfiArticle* row = [afiArticleList get:i];
                    row.siteColor = afiSite.color;
                    row.siteTextColor = afiSite.textColor;
                }
                [siteList add:afiSite];
            }
            
            [VknThreadUtil mainBlock:^{
                callback();
            }];
        }];
    }];
}

-(void) merge{
    for(int i=0,max=[siteList count];i<max;i++){
        Site* site = [siteList get:i];
        [self mergeArticleList:site];
    }
}

-(void) mergeArticleList:(Site*)site{
    ArticleList* list = [[ArticleList alloc] init];
    for(int i=0,max=[articleList count];i<max;i++){
        Article* article = [articleList get:i];
        if(site.id_ == -1){
            [list add:article];
            continue;
        }
        if(site.id_ == article.siteId){
            [list add:article];
            article.siteTextColor = site.textColor;
            article.siteColor = site.color;
            article.siteName = site.name;
        }
    }
    
    site.articleList = list;
}


-(void) updateModel:(ModelCallback_t)callback{
    [VknThreadUtil threadBlock:^{
        int lastupdated = [[NSUserDefaults standardUserDefaults] integerForKey:lastupdatedKey];
        NSString* url = [Api updateModelUrl:lastupdated];
        NSLog(@"apiUrl:%@", url);
        VknUrlLoadCommand* c = [[VknUrlLoadCommand alloc] initWithUrl:[Api updateModelUrl:lastupdated]];
        c.onComplete = ^(NSData* data){
            NSDictionary* jsonDic = [VknDataUtil dataToJson:data];
            if(jsonDic == nil) return;
            
            [[Db getInstance] updateDb:jsonDic callback:^{
                int timestamp = [[jsonDic objectForKey:@"timestamp"] intValue];
                NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
                [ud setInteger:timestamp forKey:lastupdatedKey];
                
                NSString* afiString = [VknDataUtil jsonToString:[jsonDic objectForKey:@"afi"]];
                [ud setObject:afiString forKey:afiStringKey];
                [ud setBool:YES forKey:isDataInitializedKey];
                [ud synchronize];
                [VknThreadUtil mainBlock:^{
                    callback();
                }];
            }];
            
        };
        c.onFail = ^(NSError* error){
            
        };
        
        [c execute:nil];
    }];
}

-(void) readArticle:(int)articleId{
    [VknThreadUtil threadBlock:^{
        [[Db getInstance] readArticle:articleId];
    }];
}

@end
