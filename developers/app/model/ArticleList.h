//
//  ArticleList.h
//  developers
//
//  Created by kawase yu on 2014/09/01.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "VknModelList.h"
#import "Article.h"

@interface ArticleList : VknModelList

-(Article*)get:(int)index;
-(Article*)getById:(int)id_;

@end
