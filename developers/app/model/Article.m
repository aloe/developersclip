//
//  Article.m
//  developers
//
//  Created by kawase yu on 2014/09/01.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "Article.h"

@implementation Article

@synthesize siteId;
@synthesize title;
@synthesize excerpt;
@synthesize linkUrl;
@synthesize imageUrl;
@synthesize publishedAt;
@synthesize readed;
@synthesize favorit;
@synthesize siteColor;
@synthesize siteTextColor;
@synthesize siteName;

-(void) setParams:(FMResultSet *)results{
    siteId = [results intForColumn:@"site_id"];
    title = [results stringForColumn:@"title"];
    excerpt = [results stringForColumn:@"excerpt"];
    linkUrl = [results stringForColumn:@"link_url"];
    imageUrl = [results stringForColumn:@"image_url"];
    publishedAt = [NSDate dateWithTimeIntervalSince1970:[results intForColumn:@"published_at"]];
    readed = [results boolForColumn:@"readed"];
    favorit = [results boolForColumn:@"favorit"];
}

-(BOOL) hasImage{
    return ([imageUrl length] != 0);
}

-(BOOL) isAfi{
    return NO;
}

-(BOOL) isAppAfi{
    return NO;
}

@end
