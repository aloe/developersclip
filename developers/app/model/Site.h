//
//  Site.h
//  developers
//
//  Created by kawase yu on 2014/09/01.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "VknModel.h"
#import "ArticleList.h"

@interface Site : VknModel

@property (readonly) NSString* name;
@property (readonly) NSString* excerpt;
@property (readonly) NSString* url;
@property (readonly) NSString* rssUrl;
@property (readonly) int priority;
@property ArticleList* articleList;
@property (readonly) UIColor* color;
@property (readonly) UIColor* textColor;
@property (readonly) NSString* iconUrl;

+(Site*)createAfiSite;
+(Site*)createAppAfiSite;

@end
