//
//  ArticleList.m
//  developers
//
//  Created by kawase yu on 2014/09/01.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "ArticleList.h"

@implementation ArticleList

-(VknModel*) createModel:(FMResultSet*)results{
    return [[Article alloc] initWithResults:results];
}

-(Article*)get:(int)index{
    return (Article*)[super get:index];
}

-(Article*)getById:(int)id_{
    return (Article*)[super getById:id_];
}


@end
