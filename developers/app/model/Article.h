//
//  Article.h
//  developers
//
//  Created by kawase yu on 2014/09/01.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "VknModel.h"

@interface Article : VknModel

@property (readonly) int siteId;
@property (readonly) NSString* title;
@property (readonly) NSString* excerpt;
@property (readonly) NSString* linkUrl;
@property (readonly) NSString* imageUrl;
@property (readonly) NSDate* publishedAt;
@property BOOL readed;
@property BOOL favorit;
@property UIColor* siteColor;
@property UIColor* siteTextColor;
@property NSString* siteName;

-(BOOL) hasImage;
-(BOOL) isAfi;
-(BOOL) isAppAfi;


@end
