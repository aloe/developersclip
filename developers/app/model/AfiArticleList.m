//
//  AfiArticleList.m
//  developers
//
//  Created by kawase yu on 2014/09/04.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "AfiArticleList.h"

@implementation AfiArticleList

-(id) initWithAfiArray:(NSArray*)array{
    self = [super init];
    if(self){
        for(NSDictionary* dic in array){
            AfiArticle* afiArticle = [[AfiArticle alloc] initWithAfiJson:dic];
            [self add:afiArticle];
        }
    }
    
    return self;
}

-(id) initWithChkDatalist:(NSArray*)chkDataList{
    self = [super init];
    if(self){
        for(ChkRecordData* data in chkDataList){
            AfiArticle* afiArticle = [[AfiArticle alloc] initWithChkRecordData:data];
            [self add:afiArticle];
        }
    }
    
    return self;
}

-(AfiArticle*)get:(int)index{
    return (AfiArticle*)[super get:index];
}

-(AfiArticle*)getById:(int)id_{
    return (AfiArticle*)[super getById:id_];
}

+(AfiArticleList*)createDummy{
    AfiArticleList* list = [[AfiArticleList alloc] init];
    AfiArticle* ad = [[AfiArticle alloc] initWithAfiJson:@{
                                                           @"title":@"薬剤師 年収処方箋"
                                                           , @"link_url": @"https://itunes.apple.com/us/app/yao-ji-shi-nian-shou-zhen/id899049413?l=ja&ls=1&mt=8"
                                                           , @"image_url": @"https://s3.mzstatic.com/us/r30/Purple5/v4/08/81/69/088169da-4eef-3f69-932e-7f3c90e7eb84/icon170x170.png"
                                                           }];
    [list add:ad];
    
    ad = [[AfiArticle alloc] initWithAfiJson:@{
                                               @"title":@"もう笑いました？～絶対吹くｗ爆笑スレ大量追加!"
                                               , @"link_url": @"https://itunes.apple.com/us/app/mou-xiaoimashita-jue-dui-chuikuw/id634891205?l=ja&ls=1&mt=8"
                                               , @"image_url": @"https://s5.mzstatic.com/us/r30/Purple/v4/d5/d6/98/d5d69893-14a5-6596-85d5-6a3da218e197/icon170x170.png"
                                               }];
    [list add:ad];
    
    ad = [[AfiArticle alloc] initWithAfiJson:@{
                                               @"title":@"お買い物メモ ～手書き風の可愛いメモ帳～"
                                               , @"link_url": @"https://itunes.apple.com/us/app/o-maii-wumemo-shou-shuki-fengno/id607634035?l=ja&ls=1&mt=8"
                                               , @"image_url": @"https://s2.mzstatic.com/us/r30/Purple3/v4/7e/78/8d/7e788d29-c3bc-e44e-b5d4-14a9eac918b8/icon170x170.png"
                                               }];
    [list add:ad];
    
    return list;
}

@end
