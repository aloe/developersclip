//
//  SiteList.h
//  developers
//
//  Created by kawase yu on 2014/09/01.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "VknModelList.h"
#import "Site.h"

@interface SiteList : VknModelList

-(Site*)get:(int)index;
-(Site*)getById:(int)id_;

@end
