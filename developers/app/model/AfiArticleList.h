//
//  AfiArticleList.h
//  developers
//
//  Created by kawase yu on 2014/09/04.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "ArticleList.h"
#import "AfiArticle.h"

@interface AfiArticleList : ArticleList

-(id) initWithAfiArray:(NSArray*)array;
-(id) initWithChkDatalist:(NSArray*)chkDataList;
-(AfiArticle*)get:(int)index;
-(AfiArticle*)getById:(int)id_;
+(AfiArticleList*)createDummy;

@end
