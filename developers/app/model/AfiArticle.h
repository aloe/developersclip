//
//  AfiArticle.h
//  developers
//
//  Created by kawase yu on 2014/09/04.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "Article.h"
#import "ChkRecordData.h"

@interface AfiArticle : Article

-(id) initWithAfiJson:(NSDictionary*)dic;
-(id) initWithChkRecordData:(ChkRecordData*)data;

@end
