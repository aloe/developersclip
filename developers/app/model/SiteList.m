//
//  SiteList.m
//  developers
//
//  Created by kawase yu on 2014/09/01.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "SiteList.h"

@implementation SiteList

-(VknModel*) createModel:(FMResultSet*)results{
    return [[Site alloc] initWithResults:results];
}

-(Site*)get:(int)index{
    return (Site*)[super get:index];
}

-(Site*)getById:(int)id_{
    return (Site*)[super getById:id_];
}

@end
