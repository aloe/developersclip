//
//  AfiArticle.m
//  developers
//
//  Created by kawase yu on 2014/09/04.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "AfiArticle.h"

@interface AfiArticle (){
    BOOL isAppAfi;
}

@end

@implementation AfiArticle

@synthesize title;
@synthesize linkUrl;
@synthesize imageUrl;
@synthesize siteColor;
@synthesize siteTextColor;

-(id) initWithAfiJson:(NSDictionary*)dic{
    self = [super init];
    if(self){
        title = [dic objectForKey:@"title"];
        linkUrl = [dic objectForKey:@"link_url"];
        imageUrl = [dic objectForKey:@"image_url"];
    }
    
    return self;
}

-(id) initWithChkRecordData:(ChkRecordData*)data{
    self = [super init];
    if(self){
        title = /*[NSString stringWithFormat:@"%@\n%@", data.title, data.detail]*/ data.detail;
        linkUrl = data.linkUrl;
        imageUrl = data.imageIcon;
        siteColor = UIColorFromHex(0x686868);
        siteTextColor = UIColorFromHex(0x1c1c1c);
        isAppAfi = YES;
    }
    
    return self;
}

-(BOOL) isAfi{
    return YES;
}

-(BOOL) isAppAfi{
    return isAppAfi;
}

-(BOOL) hasImage{
    return ([imageUrl length] != 0);
}

@end
