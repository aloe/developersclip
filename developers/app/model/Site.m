//
//  Site.m
//  developers
//
//  Created by kawase yu on 2014/09/01.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "Site.h"

@implementation Site

@synthesize name;
@synthesize excerpt;
@synthesize url;
@synthesize rssUrl;
@synthesize priority;
@synthesize articleList;
@synthesize color;
@synthesize textColor;
@synthesize iconUrl;

-(void) setParams:(FMResultSet*)results{
    name = [results stringForColumn:@"name"];
    excerpt = [results stringForColumn:@"excerpt"];
    url = [results stringForColumn:@"url"];
    rssUrl = [results stringForColumn:@"rss_url"];
    iconUrl = [results stringForColumn:@"icon_url"];
    priority = [results intForColumn:@"priority"];
    
    color = [VknColorUtil hexToUIColor:[results stringForColumn:@"color"]];
    textColor = [VknColorUtil hexToUIColor:[results stringForColumn:@"text_color"]];
}

-(id) initAfi{
    self = [super init];
    if(self){
        name = @"【PR】転職情報";
        excerpt = @"転職しちゃいな";
        color = UIColorFromHex(0x315fbb);
        textColor = UIColorFromHex(0x1d386e);
    }
    
    return self;
}

-(id) initAppAfi{
    self = [super init];
    if(self){
        name = @"【PR】オススメアプリ";
        excerpt = @"インストールしちゃいな。恵まれない開発者に愛の手を";
        color = UIColorFromHex(0xab2026);
        textColor = UIColorFromHex(0x5e1115);
    }
    
    return self;
}

+(Site*) createAfiSite{
    return [[Site alloc] initAfi];
}

+(Site*) createAppAfiSite{
    return [[Site alloc] initAppAfi];
}

@end
