//
//  Model.h
//  developers
//
//  Created by kawase yu on 2014/09/01.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SiteList.h"
#import "ArticleList.h"

@interface Model : NSObject

typedef void (^ModelCallback_t)();

+(Model*)getInstance;
-(void) setup:(ModelCallback_t)callback;
-(void) updateModel:(ModelCallback_t)callback;
-(void) readArticle:(int)articleId;

-(BOOL) isDataInitialized;
-(SiteList*)siteList;

@end
