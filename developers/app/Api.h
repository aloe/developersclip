//
//  Api.h
//  developers
//
//  Created by kawase yu on 2014/09/01.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Api : NSObject

+(NSString*)url:(NSString*)path;
+(NSString*)updateModelUrl:(int)lastupdated;

@end
