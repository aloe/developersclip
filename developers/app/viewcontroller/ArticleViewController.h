//
//  ArticleViewController.h
//  developers
//
//  Created by kawase yu on 2014/09/04.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "BaseViewController.h"
#import "Article.h"

@protocol ArticleViewDelegate <NSObject>

-(void) showHeader;
-(void) hideHeader;

@end

@interface ArticleViewController : BaseViewController

-(id) initWithDelegate:(NSObject<ArticleViewDelegate>*)delegate_ andArticle:(Article*)article_;

@end
