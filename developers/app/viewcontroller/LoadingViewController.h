//
//  LoadingViewController.h
//  developers
//
//  Created by kawase yu on 2014/09/05.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "BaseViewController.h"

@interface LoadingViewController : BaseViewController

-(void) startLoading;
-(void) endLoading;

@end
