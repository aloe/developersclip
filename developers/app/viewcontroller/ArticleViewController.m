//
//  ArticleViewController.m
//  developers
//
//  Created by kawase yu on 2014/09/04.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "ArticleViewController.h"
#import <Social/Social.h>
#import "VknUrlLoadCommand.h"
#import "Api.h"
#import "AdstirMraidView.h"

@interface ArticleViewController ()<
UIWebViewDelegate
, UIScrollViewDelegate>{
    __weak NSObject<ArticleViewDelegate>* delegate;
    Article* article;
    UIWebView* webView_;
    float currentY;
    float currentSpeed;
    
    AdstirMraidView* adView;
    UIView* footerView;
    UIButton* webBackButton;
    UIButton* webNextButton;
}

@end

@implementation ArticleViewController

-(id) initWithDelegate:(NSObject<ArticleViewDelegate>*)delegate_ andArticle:(Article*)article_{
    self = [super init];
    if(self){
        delegate = delegate_;
        article = article_;
        [self initialize];
    }
    
    return self;
}

-(void) initialize{
    self.view.backgroundColor = [UIColor whiteColor];
    [self setWebView];
    if(![DevUtil isJaDevice]){
        [self setAd];
    }
    [self setFooter];
}

-(void) setWebView{
    webView_ = [[UIWebView alloc] initWithFrame:CGRectMake(0, 60, 320, [VknDeviceUtil windowHeight]-60)];
    webView_.delegate = self;
    webView_.scrollView.delegate = self;
    webView_.scalesPageToFit = YES;
    [self.view addSubview:webView_];
    [self showLoading];
    [webView_ loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:article.linkUrl]]];
}

-(void) setAd{
    adView = [[AdstirMraidView alloc] initWithAdSize:kAdstirAdSize320x50 media:ADSTIR_MEDIA_ID spot:ADSTIR_SPOT_ID_MINI];
    CGRect frame = adView.frame;
    frame.origin.y = [VknDeviceUtil windowHeight] - 50;
    adView.frame = frame;
    adView.transform = CGAffineTransformMakeTranslation(0, 50.0);
    [self.view addSubview:adView];
    [adView start];
}

-(void) setFooter{
    footerView = [[UIView alloc] initWithFrame:CGRectMake(0, [VknDeviceUtil windowHeight]-40, 320, 40)];
    UIView* footerLine = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 0.5f)];
    [footerView addSubview:footerLine];
    footerView.backgroundColor = article.siteColor;
    footerLine.backgroundColor = article.siteTextColor;
    [self.view addSubview:footerView];
    
    CGRect buttonFrame = CGRectMake(0, 0, 80, 40);
    
    webBackButton = [UIButton buttonWithType:UIButtonTypeCustom];
    webBackButton.frame = buttonFrame;
    [webBackButton setImage:[UIImage imageNamed:@"webBackButton"] forState:UIControlStateNormal];
    [webBackButton addTarget:self action:@selector(tapWebBack) forControlEvents:UIControlEventTouchUpInside];
    webBackButton.enabled = NO;
    [footerView addSubview:webBackButton];
    buttonFrame.origin.x += 80;
    
    webNextButton = [UIButton buttonWithType:UIButtonTypeCustom];
    webNextButton.frame = buttonFrame;
    [webNextButton setImage:[UIImage imageNamed:@"webNextButton"] forState:UIControlStateNormal];
    [webNextButton addTarget:self action:@selector(tapWebNext) forControlEvents:UIControlEventTouchUpInside];
    webNextButton.enabled = NO;
    [footerView addSubview:webNextButton];
    buttonFrame.origin.x += 80;
    
    UIButton* reloadButotn = [UIButton buttonWithType:UIButtonTypeCustom];
    reloadButotn.frame = buttonFrame;
    [reloadButotn setImage:[UIImage imageNamed:@"webReloadButton"] forState:UIControlStateNormal];
    [reloadButotn addTarget:self action:@selector(tapWebReload) forControlEvents:UIControlEventTouchUpInside];
    [footerView addSubview:reloadButotn];
    buttonFrame.origin.x += 80;
    
    
    UIButton* shareButotn = [UIButton buttonWithType:UIButtonTypeCustom];
    shareButotn.frame = buttonFrame;
    [shareButotn setImage:[UIImage imageNamed:@"webShareButton"] forState:UIControlStateNormal];
    [shareButotn addTarget:self action:@selector(tapWebShare) forControlEvents:UIControlEventTouchUpInside];
    [footerView addSubview:shareButotn];
    buttonFrame.origin.x += 80;
}

-(void) tapWebBack{
    if([webView_ canGoBack]){
        [self showLoading];
        [webView_ goBack];
    }
}

-(void) tapWebNext{
    if([webView_ canGoForward]){
        [self showLoading];
        [webView_ goForward];
    }
}

-(void) tapWebReload{
    [self showLoading];
    [webView_ reload];
}

-(void) tapWebShare{
    NSString* title = [webView_ stringByEvaluatingJavaScriptFromString:@"document.title"];
    NSString* urlString = [webView_ stringByEvaluatingJavaScriptFromString:@"document.URL"];
    
    UIActionSheet* actionSheet = [[UIActionSheet alloc] init];
    actionSheet.delegate = self;
    actionSheet.title = [NSString stringWithFormat:@"%@\n%@", title, urlString];
    [actionSheet addButtonWithTitle:@"Twitter"];
    [actionSheet addButtonWithTitle:@"Facebook"];
    [actionSheet addButtonWithTitle:@"Line"];
    [actionSheet addButtonWithTitle:@"違反報告"];
    [actionSheet addButtonWithTitle:@"キャンセル"];
    actionSheet.cancelButtonIndex = 4;
    
    // アクションシートの表示
    [actionSheet showInView:self.view.superview];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    NSString* title = [webView_ stringByEvaluatingJavaScriptFromString:@"document.title"];
    NSString* urlString = [webView_ stringByEvaluatingJavaScriptFromString:@"document.URL"];
    
    switch (buttonIndex) {
            // twitter
        case 0:{
            SLComposeViewController* slComposeViewController = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
            [slComposeViewController setInitialText:title];
            [slComposeViewController addURL:[NSURL URLWithString:urlString]];
            [slComposeViewController addImage:[UIImage imageNamed:@"icon_120"]];
            [self presentViewController:slComposeViewController animated:YES completion:^{
                
            }];
        }
            
            break;
            // facebook
        case 1:{
            SLComposeViewController* slComposeViewController = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
            [slComposeViewController setInitialText:title];
            [slComposeViewController addURL:[NSURL URLWithString:urlString]];
            [slComposeViewController addImage:[UIImage imageNamed:@"icon_120"]];
            [self presentViewController:slComposeViewController animated:YES completion:^{
                
            }];
        }
            
            break;
            // line
        case 2:{
            NSString* lineText = [NSString stringWithFormat:@"%@ \n%@", title, urlString];
            NSString *contentType = @"text";
            NSString *contentKey = (__bridge NSString *)CFURLCreateStringByAddingPercentEscapes(
                                                                                                NULL,
                                                                                                (CFStringRef)lineText,
                                                                                                NULL,
                                                                                                (CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                                                                                kCFStringEncodingUTF8 );
            NSString *urlString2 = [NSString
                                    stringWithFormat:@"line://msg/%@/%@",
                                    contentType, contentKey];
            NSURL *url = [NSURL URLWithString:urlString2];
            [[UIApplication sharedApplication] openURL:url];
        }
            break;
            // 違反報告
        case 3:{
            NSString* path = [NSString stringWithFormat:@"ihan.php?url=%@", urlString];
            VknUrlLoadCommand* c = [[VknUrlLoadCommand alloc] initWithUrl:[Api url:path]];
            c.onComplete = ^(NSData* data){
                [self showToast:@"ご報告ありがとうございます"];
            };
            [c execute:nil];
        }
            break;
            
        default:
            break;
    }
}

- (void)actionSheetCancel:(UIActionSheet *)actionSheet{
    
}

#pragma mark -------- UIWebViewDelegate -----------

- (void)webViewDidStartLoad:(UIWebView *)webView{

}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    [self hideLoading];
    
    webBackButton.enabled = [webView canGoBack];
    webNextButton.enabled = [webView canGoForward];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    
    if (navigationType == UIWebViewNavigationTypeLinkClicked ){
        [self showLoading];
    }
    
    return YES;
}

#pragma mark -------- UISCrollViewDelegate -----------

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    currentSpeed = currentY - scrollView.contentOffset.y;
    currentY = scrollView.contentOffset.y;
}

- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView{
    NSLog(@"currentSpeed:%f", currentSpeed);
    
    if(currentSpeed < 0){
        [delegate hideHeader];
        [UIView animateWithDuration:0.2f animations:^{
            webView_.frame = CGRectMake(0, 0, 320, [VknDeviceUtil windowHeight]);
            footerView.transform = CGAffineTransformMakeTranslation(0, 60);
            
            if(![DevUtil isJaDevice])
                adView.transform = CGAffineTransformMakeTranslation(0, 0);
        }];
    }else{
        [delegate showHeader];
        [UIView animateWithDuration:0.2f animations:^{
            webView_.frame = CGRectMake(0, 60, 320, [VknDeviceUtil windowHeight]-60);
            footerView.transform = CGAffineTransformMakeTranslation(0, 0);
            
            if(![DevUtil isJaDevice])
                adView.transform = CGAffineTransformMakeTranslation(0, 50.0);
        }];
    }
}

#pragma mark -------- -----------

-(void) dealloc{
    webView_.delegate = nil;
    [webView_ removeFromSuperview];
    webView_ = nil;
}

@end
