//
//  ArticleListViewController.m
//  developers
//
//  Created by kawase yu on 2014/09/03.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "ArticleListViewController.h"
#import "ArticleCell.h"
#import "AdstirMraidView.h"

@interface ArticleListViewController ()<
UITableViewDataSource
, UITableViewDelegate
, ArticleCellDelegate
>{
    __weak NSObject<ArticleListViewDelegate>* delegate;
    UITableView* tableView;
    ArticleList* articleList;
}

@end

@implementation ArticleListViewController

-(id) initWithDelegate:(NSObject<ArticleListViewDelegate>*)delegate_{
    self = [super init];
    if(self){
        delegate = delegate_;
        [self initialize];
    }
    
    return self;
}

-(void) initialize{
    self.view.backgroundColor = [UIColor whiteColor];
//    self.view.frame = CGRectMake(0, 60, 320, [VknDeviceUtil windowHeight]-60);
    UIView* tableWrap = [[UIView alloc] initWithFrame:CGRectMake(0, 60, [VknDeviceUtil windowHeight]-60, 320)];
    tableWrap.transform = CGAffineTransformMakeRotation(M_PI * -90.0f / 180.0f);
    tableWrap.center = CGPointMake(160, ([VknDeviceUtil windowHeight]+60)/2.0f);
    
    tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, [VknDeviceUtil windowHeight]-60, 320)];
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.contentOffset = CGPointMake(0, 1);
    tableView.showsVerticalScrollIndicator = NO;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    if([DevUtil isJaDevice]){
        AdstirMraidView* adView = [[AdstirMraidView alloc] initWithAdSize:kAdstirAdSize320x100 media:ADSTIR_MEDIA_ID spot:ADSTIR_SPOT_ID];
        tableView.tableFooterView = adView;
        [adView start];
    }
    
    [tableWrap addSubview:tableView];
    
    [self.view addSubview:tableWrap];
}

#pragma mark -------- ArticleCellDelegate -----------

-(void) onSelectArticle:(Article *)article{
    [delegate onSelectArticle:article];
}

#pragma mark -------- UISCrolLViewDelegate -----------

-(void) scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    [DevUtil mainSv].scrollEnabled = NO;
}

-(void) scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    if(!decelerate){
        [DevUtil mainSv].scrollEnabled = YES;
    }
}

-(void) scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    [DevUtil mainSv].scrollEnabled = YES;
    
    float sa = scrollView.contentSize.height - (scrollView.frame.size.height + scrollView.contentOffset.y);
    if(scrollView.contentOffset.y == 0){
        [scrollView setContentOffset:CGPointMake(0, 1.0f) animated:YES];
    }else if(sa == 0){
        [scrollView setContentOffset:CGPointMake(0, scrollView.contentOffset.y-1.0) animated:YES];
    }
}

#pragma mark -------- UITableViewDelegate -----------


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    int c = [articleList count];
    if(c % 3 == 0){
        return c / 3;
    }
    return (c/3) + 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 150;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *cellIdentifier = @"Cell";
    ArticleCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[ArticleCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        [cell setDelegate:self];
    }
    
    [cell reload:articleList index:indexPath.row];
    
    return cell;
}

#pragma mark -------- public -----------

-(void) reload:(ArticleList*)articleList_{
    articleList = articleList_;
    [tableView reloadData];
    
    [tableView setContentOffset:CGPointMake(0, 1.0f) animated:YES];
}

@end
