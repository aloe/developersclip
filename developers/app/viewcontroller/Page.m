//
//  Page.m
//  developers
//
//  Created by kawase yu on 2014/09/03.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "Page.h"
#import "ArticleListViewController.h"
#import "ArticleViewController.h"
#import "Model.h"
#import "Site.h"

@interface Page ()<
ArticleListViewDelegate
, ArticleViewDelegate
>{
    __weak NSObject<PageDelegate>* delegate;
    Site* site;
    UINavigationController* navigationController;
    ArticleListViewController* articleListViewController;
    
    UIView* headerView;
    UIView* line;
    UILabel* titleLabel;
    UILabel* excerptLabel;
    UIView* iconBg;
    
    UIImageView* icon;
    UIView* blackLayer;
    
    UIView* listHeader;
    UIButton* backButton;
    UILabel* articleTitle;
}

@end

@implementation Page

-(id) initWithDelegate:(NSObject<PageDelegate>*)delegate_{
    self = [super init];
    if(self){
        delegate = delegate_;
        [self initialize];
    }
    
    return self;
}

-(void) initialize{
    articleListViewController = [[ArticleListViewController alloc] initWithDelegate:self];
    navigationController = [[UINavigationController alloc] initWithRootViewController:articleListViewController];
    [navigationController setNavigationBarHidden:YES];
    
    
    UISwipeGestureRecognizer* reco = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeRight)];
    reco.direction = UISwipeGestureRecognizerDirectionRight;
    [navigationController.view addGestureRecognizer:reco];
    
    UIView* view = navigationController.view;
    
    headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 60)];
    [view addSubview:headerView];
    
    [self setListHeader];
    [self setArticleHeader];
    
    line = [[UIView alloc] initWithFrame:CGRectMake(0, 59, 320, 1)];
    [headerView addSubview:line];
    
    blackLayer = [[UIView alloc] initWithFrame:view.frame];
    blackLayer.backgroundColor = [UIColor blackColor];
    blackLayer.alpha = 0;
    blackLayer.userInteractionEnabled = NO;
    [view addSubview:blackLayer];
}

-(void) setListHeader{
    listHeader = [[UIView alloc] initWithFrame:headerView.frame];
    [headerView addSubview:listHeader];
    
    iconBg = [[UIView alloc] init];
    iconBg.backgroundColor = [UIColor whiteColor];
    iconBg.layer.borderWidth = 0.5f;
    iconBg.layer.borderColor = UIColorFromHex(0xcccccc).CGColor;
    icon = [[UIImageView alloc] init];
    [iconBg addSubview:icon];
    [listHeader addSubview:iconBg];
    
    titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 20, 300, 20)];
    titleLabel.font = [UIFont systemFontOfSize:14.0f];
    [listHeader addSubview:titleLabel];
    
    excerptLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 38, 300, 18)];
    excerptLabel.font = [UIFont systemFontOfSize:11.0f];
    excerptLabel.numberOfLines = 2;
    excerptLabel.adjustsFontSizeToFitWidth = YES;
    [listHeader addSubview:excerptLabel];
}

-(void) setArticleHeader{
    backButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    170 × 80
    [backButton setImage:[UIImage imageNamed:@"backButton"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(tapBack) forControlEvents:UIControlEventTouchUpInside];
    backButton.frame = CGRectMake(0, 20, 85, 40);
    backButton.transform = CGAffineTransformMakeTranslation(0, 10);
    backButton.alpha = 0;
    [headerView addSubview:backButton];
    
    articleTitle = [[UILabel alloc] initWithFrame:CGRectMake(80, 20, 180, 40)];
    articleTitle.numberOfLines = 2;
    articleTitle.adjustsFontSizeToFitWidth = YES;
    articleTitle.minimumScaleFactor = 0.7f;
    articleTitle.font = [UIFont systemFontOfSize:14.0f];
    articleTitle.transform = CGAffineTransformMakeTranslation(0, 10);
    articleTitle.alpha = 0;
    
    [headerView addSubview:articleTitle];
}

-(void) tapBack{
    [navigationController popViewControllerAnimated:YES];
    [UIView animateWithDuration:0.2f animations:^{
        listHeader.transform = CGAffineTransformMakeTranslation(0, 0);
        backButton.transform = articleTitle.transform = CGAffineTransformMakeTranslation(0, 20);
        backButton.alpha = articleTitle.alpha = 0.0f;
    } completion:^(BOOL finished) {
        [DevUtil dispatchGlobalEvent:ON_HIDE_ARTICLE_EVENT];
    }];
    
    [self showHeader];
}

-(void) swipeRight{
    if([navigationController.viewControllers count] > 1)
        [self tapBack];
}

#pragma mark -------- ArtileListViewDelegate -----------

-(void) onSelectArticle:(Article *)article{
    NSLog(@"onSelect:%@", article.title);
    [DevUtil trackScreen:article.title];
    [DevUtil trackEvent:@"記事" action:@"表示" label:article.siteName value:0];
    [DevUtil dispatchGlobalEvent:ON_SHOW_ARTICLE_EVENT];
    articleTitle.text = article.title;
    [UIView animateWithDuration:0.2f animations:^{
        listHeader.transform = CGAffineTransformMakeTranslation(320-iconBg.frame.size.width-15, 0);
        backButton.transform = articleTitle.transform = CGAffineTransformMakeTranslation(0, 0);
        backButton.alpha = articleTitle.alpha = 1.0f;
    } completion:^(BOOL finished) {
        
    }];
    
    ArticleViewController* articleViewController = [[ArticleViewController alloc] initWithDelegate:self andArticle:article];
    [navigationController pushViewController:articleViewController animated:YES];
    
    [[Model getInstance] readArticle:article.id_];
}

#pragma mark -------- ArticleViewDelegate -----------

-(void) showHeader{
    [UIView animateWithDuration:0.2f animations:^{
        headerView.transform = CGAffineTransformMakeTranslation(0, 0);
    } completion:^(BOOL finished) {
        
    }];
}

-(void) hideHeader{
    [UIView animateWithDuration:0.2f animations:^{
        headerView.transform = CGAffineTransformMakeTranslation(0, -60);
    } completion:^(BOOL finished) {
        
    }];
}

#pragma mark -------- public -----------

-(UIView*) getView{
    return navigationController.view;
}

-(void) registerModel:(Site *)site_{
    site = site_;
}

-(void) reload{
    NSLog(@"reload:%@", site.name);
    line.backgroundColor = site.textColor;
    headerView.backgroundColor = site.color;
    titleLabel.text = site.name;
    titleLabel.textColor = excerptLabel.textColor = [UIColor whiteColor];
    [articleListViewController reload:site.articleList];
    
    articleTitle.textColor = site.textColor;
    excerptLabel.text = site.excerpt;
    
    icon.image = nil;
    [VknImageCache loadImage:site.iconUrl defaultImage:nil callback:^(UIImage *image, NSString *key, BOOL useCache) {
        image = [VknImageUtil resizeImage:image height:29];
        float x = image.size.width + 10;
        [VknThreadUtil mainBlock:^{
            titleLabel.transform = CGAffineTransformMakeTranslation(x, 0);
            excerptLabel.frame = CGRectMake(10+x, 38, 300-x, 18);
            icon.image = image;
            icon.frame = CGRectMake(2, 2, image.size.width, image.size.height);
            iconBg.frame = CGRectMake(10, 22.5f, image.size.width + 4, image.size.height + 4);
        }];
    }];
}

-(void) dark:(float)rate{
    blackLayer.alpha = rate * 5.0f;
}

@end
