//
//  ArticleListViewController.h
//  developers
//
//  Created by kawase yu on 2014/09/03.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "BaseViewController.h"
#import "ArticleList.h"

@protocol ArticleListViewDelegate <NSObject>

-(void) onSelectArticle:(Article*)article;

@end

@interface ArticleListViewController : BaseViewController

-(id) initWithDelegate:(NSObject<ArticleListViewDelegate>*)delegate_;
-(void) reload:(ArticleList*)articleList_;

@end
