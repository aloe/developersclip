//
//  LoadingViewController.m
//  developers
//
//  Created by kawase yu on 2014/09/05.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "LoadingViewController.h"

@interface LoadingViewController (){
    UIImageView* bgImage;
    UILabel* loadingLabel;
}

@end

@implementation LoadingViewController

-(id) init{
    self = [super init];
    if(self){
        [self initialize];
    }
    
    return self;
}

-(void) initialize{
    UIView* view = self.view;
    bgImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"splash-1136"]];
    [view addSubview:bgImage];
    
    loadingLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 340, 320, 30)];
    loadingLabel.textColor = [UIColor whiteColor];
    loadingLabel.text = @"loading";
    loadingLabel.alpha = 0;
    loadingLabel.textAlignment = NSTextAlignmentCenter;
    loadingLabel.transform = CGAffineTransformMakeTranslation(0, 20);
    [view addSubview:loadingLabel];
}

-(void) startLoading{
    [UIView animateWithDuration:0.2f animations:^{
        loadingLabel.transform = CGAffineTransformMakeTranslation(0, 0);
        loadingLabel.alpha = 1.0f;
    } completion:^(BOOL finished) {
        
    }];
}

-(void) endLoading{
    [UIView animateWithDuration:0.2f animations:^{
        bgImage.alpha = 0.0f;
        loadingLabel.transform = CGAffineTransformMakeTranslation(0, -20);
        loadingLabel.alpha = 0.0f;
    } completion:^(BOOL finished) {
        [self.view removeFromSuperview];
    }];
}

@end
