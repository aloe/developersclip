//
//  Page.h
//  developers
//
//  Created by kawase yu on 2014/09/03.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Site.h"

@protocol PageDelegate <NSObject>

@end

@interface Page : NSObject

-(id) initWithDelegate:(NSObject<PageDelegate>*)delegate_;
-(UIView*)getView;
-(void) registerModel:(Site*)site_;
-(void) reload;
-(void) dark:(float)rate;

@end
