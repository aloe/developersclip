//
//  Pager.m
//  developers
//
//  Created by kawase yu on 2014/09/05.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "Pager.h"

@interface Pager (){
    UIView* view;
    UIView* bgView;
    NSArray* pottiList;
}

@end

@implementation Pager

-(id) init{
    self = [super init];
    if(self){
        view = [[UIView alloc] init];
        bgView = [[UIView alloc] init];
        bgView.layer.cornerRadius = 6;
        bgView.clipsToBounds = YES;
        bgView.backgroundColor = UIColorFromHex(0xffffff);
        bgView.alpha = 0.7f;
        [view addSubview:bgView];
        
        pottiList = [[NSArray alloc] init];
    }
    
    return self;
}

-(UIView*)getView{
    return view;
}

-(void) reload:(SiteList*)siteList{
    for(UIView* v in pottiList){
        [v removeFromSuperview];
    }
    
    NSMutableArray* tmp = [[NSMutableArray alloc] init];
    CGRect pottiFrame = CGRectMake(2.5f, 4, 7, 7);
    for(int i=0,max=[siteList count];i<max;i++){
        Site* site = [siteList get:i];
        UIView* potti = [[UIView alloc] initWithFrame:pottiFrame];
//        potti.backgroundColor = UIColorFromHex(0x51b1cc);
        potti.backgroundColor = site.color;
        potti.layer.cornerRadius = 3.5f;
        [view addSubview:potti];
        [tmp addObject:potti];
        pottiFrame.origin.y += pottiFrame.size.height + 4;
    }
    
    pottiList = [NSArray arrayWithArray:tmp];
    
    view.frame = CGRectMake(320-20, 68, 12, pottiFrame.origin.y);
    bgView.frame = CGRectMake(0, 0, 12, pottiFrame.origin.y);
}

-(void) active:(int)index{
    for(int i=0,max=[pottiList count];i<max;i++){
        UIView* potti = [pottiList objectAtIndex:i];
        potti.alpha = (index == i) ? 1.0f : 0.2f;
    }
}

-(void) show{
    view.hidden = NO;
    view.transform = CGAffineTransformMakeTranslation(0, 20);
    [UIView animateWithDuration:0.2f animations:^{
        view.transform = CGAffineTransformMakeTranslation(0, 0);
        view.alpha = 1.0f;
    }];
}

-(void) hide{
    [UIView animateWithDuration:0.2f animations:^{
        view.transform = CGAffineTransformMakeTranslation(0, -20);
        view.alpha = 0.0f;
    } completion:^(BOOL finished) {
        view.hidden = YES;
    }];
}

@end
