//
//  ArticleCell.h
//  developers
//
//  Created by kawase yu on 2014/09/03.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ArticleList.h"

@protocol ArticleCellDelegate <NSObject>

-(void) onSelectArticle:(Article*)article;

@end

@interface ArticleCell : UITableViewCell

-(void) setDelegate:(NSObject<ArticleCellDelegate>*)delegate_;
-(void) reload:(ArticleList*)articleList index:(int)index;

@end
