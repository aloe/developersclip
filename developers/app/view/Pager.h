//
//  Pager.h
//  developers
//
//  Created by kawase yu on 2014/09/05.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SiteList.h"

@interface Pager : NSObject

-(UIView*)getView;
-(void) reload:(SiteList*)siteList;
-(void) active:(int)index;

-(void) show;
-(void) hide;

@end
