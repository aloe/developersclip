//
//  ArticleBlock.h
//  developers
//
//  Created by kawase yu on 2014/09/03.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Article.h"

@protocol ArticleBlockDelegate <NSObject>

-(void) onSelectArticle:(Article*)article;

@end

@interface ArticleBlock : UIView

-(id) initWithDelegate:(NSObject<ArticleBlockDelegate>*)delegate_;
-(void) reload:(Article*)article;
-(void) colorBg;
-(void) empty;

@end
