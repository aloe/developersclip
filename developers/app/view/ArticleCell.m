//
//  ArticleCell.m
//  developers
//
//  Created by kawase yu on 2014/09/03.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "ArticleCell.h"
#import "ArticleBlock.h"
#import "Ad.h"

@interface ArticleCell ()<
ArticleBlockDelegate
>{
    __weak NSObject<ArticleCellDelegate>* delegate;
    NSArray* blockList;
}

@end

@implementation ArticleCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self initialize];
    }
    return self;
}

-(void) initialize{
    self.contentView.transform = CGAffineTransformMakeRotation(M_PI * (90) / 180.0f);
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    NSMutableArray* tmp = [[NSMutableArray alloc] init];
    for(int i=0;i<3;i++){
        ArticleBlock* block = [[ArticleBlock alloc] initWithDelegate:self];
        [tmp addObject:block];
        block.transform = CGAffineTransformMakeTranslation(0, [DevUtil articleHeight]*i);
        [self.contentView addSubview:block];
    }
        
    blockList = [NSArray arrayWithArray:tmp];
}

-(void) setDelegate:(NSObject<ArticleCellDelegate>*)delegate_{
    delegate = delegate_;
}

-(void) reload:(ArticleList*)articleList index:(int)index{
    int start = index*3;
    int end = start + 3;
    int count = 0;
    BOOL odd = (index % 2 == 0);
    for(int i=start;i<end;i++){
        ArticleBlock* block = (ArticleBlock*)[blockList objectAtIndex:count];
        if(i >= [articleList count]){
            count++;
            AfiArticle* afiArticle = [[Ad getInstance] afiArticle];
            if(afiArticle != nil){
                [block reload:afiArticle];
                continue;
            }
            [block empty];
            
            continue;
        }
        Article* article = [articleList get:i];
        [block reload:article];
        if(odd){
            if(count==1){
                [block colorBg];
            }
        }else{
            if(count == 0 || count == 2){
                [block colorBg];
            }
        }
        count++;
    }
}

#pragma mark -------- ArticleBlockDelegate -----------

-(void) onSelectArticle:(Article *)article{
    [delegate onSelectArticle:article];
}


@end
