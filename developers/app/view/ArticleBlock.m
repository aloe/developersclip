//
//  ArticleBlock.m
//  developers
//
//  Created by kawase yu on 2014/09/03.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "ArticleBlock.h"

@interface ArticleBlock ()<UIAlertViewDelegate>{
    __weak NSObject<ArticleBlockDelegate>* delegate;
    Article* article;
    UIImageView* imageView;
    UILabel* titleLabel;
    UILabel* publishedLabel;
    UIView* publishedLabelBg;
    UIView* bgView;
    UIView* lightView;
    UIActivityIndicatorView* indicator;
    UILabel* largeTitleLabel;
    
    UIView* emptyView;
    UIView* siteTitle;
}

@end

@implementation ArticleBlock

-(id) initWithDelegate:(NSObject<ArticleBlockDelegate>*)delegate_{
    self = [super init];
    if(self){
        delegate = delegate_;
        [self initialize];
    }
    return self;
}

-(void) initialize{
    CGRect frame = CGRectMake(0, 0, 150, [DevUtil articleHeight]);
    self.frame = frame;
    
    bgView = [[UIView alloc] initWithFrame:frame];
    bgView.alpha = 0.0f;
    [self addSubview:bgView];
    
    lightView = [[UIView alloc] initWithFrame:frame];
    lightView.alpha = 0.0f;
    [self addSubview:lightView];
    
    imageView = [[UIImageView alloc] initWithFrame:CGRectMake(5, 5, 140, 100)];
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    imageView.clipsToBounds = YES;
    
    largeTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, 130, 90)];
    largeTitleLabel.textColor = UIColorFromHex(0xdcdcdc);
    largeTitleLabel.numberOfLines = 3;
    largeTitleLabel.font = [UIFont systemFontOfSize:16.0f];
    [imageView addSubview:largeTitleLabel];
    
    indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    indicator.frame = imageView.frame;
    [self addSubview:indicator];
    
    [self addSubview:imageView];
    
    titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 110, 140, 55)];
    titleLabel.font = [UIFont boldSystemFontOfSize:14.0f];
    titleLabel.numberOfLines = 0;
    
    if([VknDeviceUtil windowHeight] < 568){
        titleLabel.frame = CGRectMake(5, 110, 140, 25);
        titleLabel.font = [UIFont systemFontOfSize:10.0f];
    }
    
    [self addSubview:titleLabel];
    
    publishedLabelBg = [[UIView alloc] initWithFrame:CGRectMake(150-40, 10, 40, 15)];
    publishedLabelBg.alpha = 0.8f;
    [self addSubview:publishedLabelBg];
    publishedLabel = [[UILabel alloc] initWithFrame:publishedLabelBg.frame];
    publishedLabel.textColor = [UIColor whiteColor];
    publishedLabel.font = [UIFont systemFontOfSize:10.0f];
    publishedLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:publishedLabel];
    
    emptyView = [[UIView alloc] initWithFrame:frame];
    emptyView.backgroundColor = UIColorFromHex(0x777777);
    [self addSubview:emptyView];
    
    UIView* line = [[UIView alloc] initWithFrame:CGRectMake(0, [DevUtil articleHeight]-0.5f, 150, 0.5f)];
    line.backgroundColor = UIColorFromHex(0xcccccc);
    [self addSubview:line];
    
    line = [[UIView alloc] initWithFrame:CGRectMake(149.5f, 0, 0.5f, [DevUtil articleHeight])];
    line.backgroundColor = UIColorFromHex(0xcccccc);
    [self addSubview:line];
    
    [self addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap)]];
    
}

-(void) tap{
    if(article == nil) return;
    
    if([article isAfi]){
        if(![DevUtil isJaDevice]) return;
        UIAlertView* alertView = [[UIAlertView alloc] init];
        alertView.delegate = self;
        alertView.message = ([article isAppAfi]) ? @"AppStoreへ遷移します" : @"ブラウザを起動します";
        [alertView addButtonWithTitle:@"キャンセル"];
        [alertView addButtonWithTitle:@"OK"];
        alertView.cancelButtonIndex = 0;
        [alertView show];
        return;
    }
    article.readed = YES;
    lightView.alpha = 1.0f;
    [UIView animateWithDuration:0.4f animations:^{
        lightView.alpha = 0;
    } completion:^(BOOL finished) {
        publishedLabel.hidden = publishedLabelBg.hidden = YES;
        titleLabel.textColor = UIColorFromHex(0x999999);
    }];
    
    [delegate onSelectArticle:article];
}

-(void) reload:(Article*)article_{
    article = article_;
    emptyView.hidden = YES;
    
    publishedLabel.hidden = publishedLabelBg.hidden = article.readed;
    titleLabel.textColor = (article.readed) ? UIColorFromHex(0x999999) : UIColorFromHex(0x252525);
    titleLabel.text = article.title;
    publishedLabelBg.backgroundColor = article.siteColor;
    publishedLabel.text = [VknDateUtil dateToString:article.publishedAt format:@"MM/dd"];
    
    bgView.alpha = 0;
    bgView.backgroundColor = lightView.backgroundColor = article.siteColor;
    
    imageView.image = nil;
    imageView.backgroundColor = [UIColor clearColor];
    imageView.layer.cornerRadius = 0;
    if([article isAfi]){
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        publishedLabel.text = @"【PR】";
        
        if(![DevUtil isJaDevice]){
            emptyView.hidden = NO;
        }
    }
    
    largeTitleLabel.text = @"";
    if(![article hasImage]){
        float hue = (float) arc4random_uniform(100) / 100.0f;
        imageView.backgroundColor = [UIColor colorWithHue:hue saturation:0.5f brightness:0.5f alpha:1.0f];
        imageView.alpha = 1.0f;
        largeTitleLabel.text = [article.excerpt stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        return;
    }
    
    [indicator startAnimating];
    imageView.alpha = 0;
//    imageView.transform = CGAffineTransformMakeTranslation(0, 10);
    imageView.transform = CGAffineTransformMakeScale(0.9f, 0.9f);
    [VknImageCache loadImage:article.imageUrl defaultImage:nil callback:^(UIImage *image, NSString *key, BOOL useCache) {
        if(![article.imageUrl isEqualToString:key]) return;
        [VknThreadUtil mainBlock:^{
            imageView.image = image;
            [indicator stopAnimating];
            if(useCache){
                imageView.alpha = 1.0;
//                imageView.transform = CGAffineTransformMakeTranslation(0, 0);
                imageView.transform = CGAffineTransformMakeScale(1.0f, 1.0f);
                return;
            }
            [UIView animateWithDuration:0.1f animations:^{
                imageView.alpha = 1.0;
//                imageView.transform = CGAffineTransformMakeTranslation(0, 0);
                imageView.transform = CGAffineTransformMakeScale(1.0f, 1.0f);
            }];
        }];
    }];
}

-(void) colorBg{
    bgView.alpha = 0.05f;
}

-(void) empty{
    article = nil;
    emptyView.hidden = NO;
}

#pragma mark -------- UIAlertViewDelegate -----------

-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(buttonIndex != 1) return;
    [VknUtils openBrowser:article.linkUrl];
}

@end
