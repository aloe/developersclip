//
//  Ad.m
//  smart2ch
//
//  Created by kawase yu on 2014/08/11.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "Ad.h"
#import "ChkControllerDelegate.h"
#import "ChkController.h"
#import <PAKit/PopAd.h>
#import "AdstirNativeAd.h"

static ChkController* chkController;
static AfiArticleList* adcropsArticleList;
static AfiArticleList* appliPromotionArticleList;
static Site* adSite;

@interface ChkDelegate:NSObject<ChkControllerDelegate>

@end

@implementation ChkDelegate

- (void) chkControllerDataListWithSuccess:(NSDictionary *)data{
    NSLog(@"chkControllerDataListWithSuccess");
    if([chkController hasNextData]){
        [VknThreadUtil threadBlock:^{
            [chkController requestDataList];
        }];
    }else{
        NSLog(@"endAdcropsData");
    }
    
    NSArray* dataList = chkController.dataList;
    if([dataList count] == 0) return;
    adcropsArticleList = [[AfiArticleList alloc] initWithChkDatalist:dataList];
    adSite.articleList = adcropsArticleList;
}

- (void) chkControllerDataListWithError:(NSError*)error{
    
}

- (void) chkControllerDataListWithNotFound:(NSDictionary *)data{

}

@end

@implementation Ad

static ChkDelegate* chkDelegate;

static UIViewController* rootViewController;

+ (Ad*)getInstance {
    static Ad* sharedSingleton;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedSingleton = [[Ad alloc]
                           initSharedInstance];
    });
    return sharedSingleton;
}

- (id)initSharedInstance {
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (id)init {
    [self doesNotRecognizeSelector:_cmd];
    return nil;
}

-(void) setup:(UIViewController*)rootViewController_{
    rootViewController = rootViewController_;
    adcropsArticleList = [AfiArticleList createDummy];
    
    adSite = [Site createAppAfiSite];
    adSite.articleList = adcropsArticleList;
}

-(void) reload{
    adcropsArticleList = [AfiArticleList createDummy];
    
    chkDelegate = [[ChkDelegate alloc] init];
    chkController = [[ChkController alloc] initWithDelegate:chkDelegate];
    [chkController requestDataList];
}

-(AfiArticleList*) afiArticleList{
    return adcropsArticleList;
}

-(AfiArticle*)afiArticle{
    int r = arc4random_uniform([adcropsArticleList count]);
    return [adcropsArticleList get:r];
}

-(Site*)adSite{
    return adSite;
}

@end
