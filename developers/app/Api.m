//
//  Api.m
//  developers
//
//  Created by kawase yu on 2014/09/01.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "Api.h"

#define API_ENDPOINT @"http://appnews.nowhappy.info/api"

@implementation Api

+(NSString*)url:(NSString*)path{
    return [NSString stringWithFormat:@"%@/%@", API_ENDPOINT, path];
}

+(NSString*)updateModelUrl:(int)lastupdated{
    NSString* path = [NSString stringWithFormat:@"?lastupdated=%d", lastupdated];
    return [self url:path];
}

@end
