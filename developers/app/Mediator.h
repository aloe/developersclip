//
//  Mediator.h
//  developers
//
//  Created by kawase yu on 2014/09/01.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Mediator : NSObject

-(id) initWithWindow:(UIWindow*)window_;
-(void) willEnterForeground;
-(void) didEnterBackground;
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *) token;
-(void) didReceiveMemoryWarning;


@end
