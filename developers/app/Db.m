//
//  Db.m
//  developers
//
//  Created by kawase yu on 2014/09/01.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "Db.h"
#import "FMDatabase.h"
#import "FMDatabaseQueue.h"


@implementation Db

+ (Db*)getInstance {
    static Db* sharedSingleton;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedSingleton = [[Db alloc]
                           initSharedInstance];
    });
    return sharedSingleton;
}

- (id)initSharedInstance {
    self = [super init];
    if (self) {
        [self createTables];
    }
    return self;
}

- (id)init {
    [self doesNotRecognizeSelector:_cmd];
    return nil;
}

-(FMDatabase*) db{
    return [[FMDatabase alloc] initWithPath:[self dbFilePath]];
}

-(FMDatabaseQueue*)dbQueue{
    return [[FMDatabaseQueue alloc] initWithPath:[self dbFilePath]];
}

-(NSString*) dbFilePath{
    return [VknFileUtil documentFilePath:[self dbFileName]];
}

-(BOOL) hasError:(FMDatabase*)db{
    return ([db lastErrorCode] != 0);
}

-(void) showError:(FMDatabase*)db{
    if(![self hasError:db]) return;
    NSLog(@"dbError:%d: %@", [db lastErrorCode], [db lastErrorMessage]);
}

-(NSString*)dbFileName{
    return @"kowai.db";
}

#pragma mark -------- update -----------

static NSString* tableInitializedKey = @"tableInitializedKey";

-(void) createTables{
    NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
    if([ud boolForKey:tableInitializedKey]) return;
    
    FMDatabase* db = [self db];
    [db open];
    
    NSString* article = @"CREATE TABLE IF NOT EXISTS article ("
    " id INTEGER PRIMARY KEY NOT NULL "
    ", site_id INTEGER "
    ", title TEXT "
    ", excerpt TEXT "
    ", link_url TEXT "
    ", image_url TEXT "
    ", published_at INTEGER DEFAULT 0 "
    ", readed INTEGER DEFAULT 0 "
    ", favorit INTEGER DEFAULT 0 "
    ")";
    
    NSString* site = @"CREATE TABLE IF NOT EXISTS site ("
    " id INTEGER PRIMARY KEY NOT NULL "
    ", name TEXT "
    ", excerpt TEXT "
    ", url TEXT "
    ", rss_url TEXT "
    ", icon_url TEXT "
    ", color TEXT "
    ", text_color TEXT "
    ", priority INTEGER "
    ")";
    
    [db executeUpdate:article];
    [self showError:db];
    [db executeUpdate:site];
    [self showError:db];
    [db close];
    
    NSLog(@"createdTable");
    
    [ud setBool:YES forKey:tableInitializedKey];
    [ud synchronize];
}

-(void) updateDb:(NSDictionary*)dic callback:(DbCallback_t)callback{
    
    // article ------------------------
    NSLog(@"update article ------------------");
    NSDictionary* article = [dic objectForKey:@"article"];
    NSArray* createdArticleList = article[@"created"];
    NSString* insertArticleSql = @"INSERT INTO article "
    " (id, site_id, title, excerpt, link_url, image_url, published_at)"
    " VALUES "
    " (?, ?, ?, ?, ?, ?, ?)";
    
    FMDatabase* db = [self db];
    [db open];
    [db beginDeferredTransaction];
    for(NSDictionary* dic in createdArticleList){
        [db executeUpdate:insertArticleSql,
            [dic objectForKey:@"id"]
         , [dic objectForKey:@"site_id"]
         , [dic objectForKey:@"title"]
         , [dic objectForKey:@"excerpt"]
         , [dic objectForKey:@"link_url"]
         , [dic objectForKey:@"image_url"]
         , [dic objectForKey:@"published_at"]
          ];
        [self showError:db];
    }
    
    // site -----------------------------
    NSLog(@"update site ------------------");
    NSDictionary* site = [dic objectForKey:@"site"];
    NSArray* createdSiteList = [site objectForKey:@"created"];
    NSString* insertSiteSql = @"INSERT INTO site "
    " (id, name, excerpt, url, rss_url, icon_url, color, text_color, priority)"
    " VALUES "
    " (?, ?, ?, ?, ?, ?, ?, ?, ?)";
    for(NSDictionary* dic in createdSiteList){
        [db executeUpdate:insertSiteSql
         , [dic objectForKey:@"id"]
         , [dic objectForKey:@"name"]
         , [dic objectForKey:@"excerpt"]
         , [dic objectForKey:@"url"]
         , [dic objectForKey:@"rss_url"]
         , [dic objectForKey:@"icon_url"]
         , [dic objectForKey:@"color"]
         , [dic objectForKey:@"text_color"]
         , [dic objectForKey:@"priority"]
         ];
        [self showError:db];
    }
    
    
    NSArray* updatedSiteList = [site objectForKey:@"updated"];
    NSString* updateSiteSql = @"UPDATE site SET "
    " name = ?, excerpt = ?, url = ?, rss_url = ?, icon_url = ? "
    ", color = ?, text_color = ?, priority = ? "
    " WHERE id = ?";
    for(NSDictionary* dic in updatedSiteList){
        [db executeUpdate:updateSiteSql
         , [dic objectForKey:@"name"]
         , [dic objectForKey:@"excerpt"]
         , [dic objectForKey:@"url"]
         , [dic objectForKey:@"rss_url"]
         , [dic objectForKey:@"icon_url"]
         , [dic objectForKey:@"color"]
         , [dic objectForKey:@"text_color"]
         , [dic objectForKey:@"priority"]
         , [dic objectForKey:@"id"]
         ];
        [self showError:db];
        
        NSLog(@"update:%@", [dic objectForKey:@"name"]);
    }
    
    [db commit];
    
    callback();
}

#pragma mark -------- load -----------

-(void) loadSiteList:(DbResultCallback_t)callback{
    [[self dbQueue] inDatabase:^(FMDatabase *db) {
        NSString* sql = @"SELECT * FROM site ORDER BY priority DESC";
        FMResultSet* results = [db executeQuery:sql];
        [self showError:db];
        callback(results);
        [results close];
    }];
}

-(void) setup:(DbSetupCallback_t)callback{
    [[self dbQueue] inDatabase:^(FMDatabase *db) {
        NSString* sql = @"SELECT * FROM site ORDER BY priority DESC, id DESC";
        FMResultSet* siteResults = [db executeQuery:sql];
        [self showError:db];
        
        sql = @"SELECT * FROM article ORDER BY published_at DESC";
        FMResultSet* articleResult = [db executeQuery:sql];
        [self showError:db];
        
        callback(siteResults, articleResult);
        
        [siteResults close];
        [articleResult close];
    }];
}

-(void) loadArticleList:(DbResultCallback_t)callback{
    [[self dbQueue] inDatabase:^(FMDatabase *db) {
        NSString* sql = @"SELECT * FROM article ORDER BY published_at DESC";
        FMResultSet* results = [db executeQuery:sql];
        [self showError:db];
        callback(results);
        [results close];
    }];
}

-(void) readArticle:(int) articleId{
    [[self dbQueue] inDatabase:^(FMDatabase *db) {
        NSString* sql = @"UPDATE article SET readed = 1 WHERE id = ?";
        [db executeUpdate:sql, [NSNumber numberWithInteger:articleId]];
        [self showError:db];
    }];
}

@end
