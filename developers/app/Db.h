//
//  Db.h
//  developers
//
//  Created by kawase yu on 2014/09/01.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FMResultSet.h"

@interface Db : NSObject

typedef void (^DbCallback_t)();
typedef void (^DbResultCallback_t)(FMResultSet* result);
typedef void (^DbSetupCallback_t)(FMResultSet* siteResult, FMResultSet* articleResult);


+(Db*)getInstance;
-(void) updateDb:(NSDictionary*)dic callback:(DbCallback_t)callback;
-(void) loadSiteList:(DbResultCallback_t)callback;
-(void) loadArticleList:(DbResultCallback_t)callback;

-(void) setup:(DbSetupCallback_t)callback;
-(void) readArticle:(int) articleId;

@end
